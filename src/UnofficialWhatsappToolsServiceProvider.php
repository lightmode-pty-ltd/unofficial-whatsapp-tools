<?php

namespace UnofficialWhatsappTools;

use Illuminate\Support\ServiceProvider;

class UnofficialWhatsappToolsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(\Illuminate\Routing\Router $router)
    {
        if (config('unofficial-whatsapp.router.includeRoutes')) {
            $router->prefix(config('unofficial-whatsapp.router.prefix'))
                ->namespace('UnofficialWhatsappTools\Http\Controllers')
                ->middleware(config('unofficial-whatsapp.router.middleware', []))
                ->group(__DIR__ . '/Http/api.php');
        }

        $argv = $this->app->request->server->get('argv');
        if (isset($argv[1]) and $argv[1] == 'vendor:publish') {
            $this->publishes([
                __DIR__ . '/../config/unofficial-whatsapp.php' => config_path('unofficial-whatsapp.php'),
            ], 'config');
            $this->publishes([
                __DIR__ . '/UnofficialWhatsappMessage.php.stub' => app_path('UnofficialWhatsappMessage.php'),
            ], 'model');

            $existing = glob(database_path('migrations/*_create_unofficial_whatsapp_*'));
            if (empty($existing)) {
                $this->publishes([
                    __DIR__ . '/../database/migrations/create_unofficial_whatsapp_messages.php.stub' => database_path('migrations/' . date('Y_m_d_His', time()) . '1_create_unofficial_whatsapp_messages.php')
                ], 'migrations');
            }
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/unofficial-whatsapp.php', 'unofficial-whatsapp');

        $this->app->bind('command.unofficialwhatsapptools:setup', Commands\SetupCommand::class);

        $this->commands([
            'command.unofficialwhatsapptools:setup',
        ]);

    }

}
