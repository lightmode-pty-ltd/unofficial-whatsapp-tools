<?php

namespace UnofficialWhatsappTools;

use Illuminate\Database\Eloquent\Model;

class UnofficialWhatsappMessage extends UnofficialWhatsappMessageTypes
{
    protected $table = 'unofficial_whatsapp_messages';

    const WEBHOOK_TYPE_WEBHOOK_TEST = 1;
    const WEBHOOK_TYPE_MESSAGE_QUEUED = 2;
    const WEBHOOK_TYPE_DELIVERY_REPORT = 3;
    const WEBHOOK_TYPE_MESSAGE_RECEIVED = 4;

    public function getDetailsAttribute()
    {
        return json_decode($this->attributes['details'], true);
    }

    public function setDetailsAttribute($details)
    {
        $this->attributes['details'] = json_encode($details);
    }

    public static function queueTextMessage(int $recipient, $content, $sender = null)
    {
        $message = new self();
        $message->type = self::MESSAGE_TYPE_TEXT;
        $message->content = $content;
        $message->recipient = $recipient;
        $message->sender = $sender;
        $message->details = [];
        $message->created_at = now();
        $message->save();

        dispatch(new \UnofficialWhatsappTools\Jobs\SendToGateway($message->id));

        return $message;
    }

    public static function queueAttachment(int $recipient, $url, $fileName, $content = '', $sender = null)
    {
        $message = new self();
        $message->type = self::MESSAGE_TYPE_MEDIA;
        $message->content = $content;
        $message->recipient = $recipient;
        $message->sender = $sender;
        $message->details = [
            'url' => $url,
            'file_name' => $fileName
        ];
        $message->created_at = now();
        $message->save();

        dispatch(new \UnofficialWhatsappTools\Jobs\SendToGateway($message->id));

        return $message;
    }

    public static function queueLocationPin(int $recipient, $latitude, $longitude, $locationName = null, $locationAddress = null, $locationUrl = null, $sender = null)
    {
        $message = new self();
        $message->type = self::MESSAGE_TYPE_LOCATION;
        $message->content = '';
        $message->recipient = $recipient;
        $message->sender = $sender;
        $message->details = [
            'latitude' => $latitude,
            'longitude' => $longitude,
            'location_name' => $locationName,
            'location_address' => $locationAddress,
            'location_url' => $locationUrl,
        ];
        $message->created_at = now();
        $message->save();

        dispatch(new \UnofficialWhatsappTools\Jobs\SendToGateway($message->id));

        return $message;
    }

    public static function queueButtons(int $recipient, $buttons, $buttonsTitle = '', $buttonsBody = '', $buttonsFooter = '', $sender = null)
    {
        $message = new self();
        $message->type = self::MESSAGE_TYPE_BUTTONS;
        $message->content = '';
        $message->recipient = $recipient;
        $message->sender = $sender;
        $message->details = [
            'buttons' => $buttons,
            'button_title' => $buttonsTitle,
            'button_body' => $buttonsBody,
            'button_footer' => $buttonsFooter,
        ];
        $message->created_at = now();
        $message->save();

        dispatch(new \UnofficialWhatsappTools\Jobs\SendToGateway($message->id));

        return $message;
    }

    public static function queuePoll(int $recipient, $pollQuestion, $pollAnswers, $pollAllowMultipleAnswers = false, $sender = null)
    {
        $message = new self();
        $message->type = self::MESSAGE_TYPE_POLL;
        $message->content = '';
        $message->recipient = $recipient;
        $message->sender = $sender;
        $message->details = [
            'question' => $pollQuestion,
            'answers' => $pollAnswers,
            'allow_multiple_answers' => $pollAllowMultipleAnswers,
        ];
        $message->created_at = now();
        $message->save();

        dispatch(new \UnofficialWhatsappTools\Jobs\SendToGateway($message->id));

        return $message;
    }

    public static function queueVCard(int $recipient, $contactName, $contactNumber, $sender = null)
    {
        $message = new self();
        $message->type = self::MESSAGE_TYPE_VCARD;
        $message->content = '';
        $message->recipient = $recipient;
        $message->sender = $sender;
        $message->details = [
            'contact_name' => $contactName,
            'contact_number' => $contactNumber,
        ];
        $message->created_at = now();
        $message->save();

        dispatch(new \UnofficialWhatsappTools\Jobs\SendToGateway($message->id));

        return $message;
    }

    public static function queueBotMessage(int $recipient, $content, $sender = null)
    {
        $message = new self();
        $message->type = self::MESSAGE_TYPE_AUTOMATED;
        $message->content = $content;
        $message->recipient = $recipient;
        $message->sender = $sender;
        $message->details = [];
        $message->created_at = now();
        $message->save();

        dispatch(new \UnofficialWhatsappTools\Jobs\SendToGateway($message->id));

        return $message;
    }

    public static function setWebhook()
    {
        $requestData = [
            'url' => route(config('unofficial-whatsapp.router.namedPrefix') . '.' . config('unofficial-whatsapp.router.webhookEndpoint')),
        ];

        $session = curl_init(config('unofficial-whatsapp.api.endpoint') . '/webhook');
        curl_setopt($session, CURLOPT_POST, true);
        curl_setopt($session, CURLOPT_POSTFIELDS, $requestData);

        curl_setopt($session, CURLOPT_HTTPHEADER, [
            'x-api-key: ' . config('unofficial-whatsapp.api.key'),
            'x-api-secret: ' . config('unofficial-whatsapp.api.secret')
        ]);

        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($session, CURLOPT_FOLLOWLOCATION, true);
        $response = curl_exec($session);
        curl_close($session);

        return json_decode($response, true);
    }

    public function sendToGateway()
    {
        if (!$this->queued) {
            $sender = $this->sender;

            if (is_null($sender)) {
                $sender = config('unofficial-whatsapp.api.number');
            }

            switch ($this->type) {
                case self::MESSAGE_TYPE_TEXT:
                case self::MESSAGE_TYPE_AUTOMATED:
                case self::MESSAGE_TYPE_SYSTEM:
                    $requestData = [
                        'recipient' => $this->recipient,
                        'content' => $this->content,
                        'sender' => $sender,
                    ];

                    $url = config('unofficial-whatsapp.api.endpoint') . '/sendMessage';
                    break;

                case self::MESSAGE_TYPE_MEDIA:
                    $details = $this->details;

                    $requestData = [
                        'recipient' => $this->recipient,
                        'content' => $this->content,
                        'file_url' => $details['url'],
                        'file_name' => $details['file_name'],
                        'sender' => $sender,
                    ];

                    $url = config('unofficial-whatsapp.api.endpoint') . '/sendMedia';
                    break;

                case self::MESSAGE_TYPE_LOCATION:
                    $details = $this->details;

                    $requestData = [
                        'recipient' => $this->recipient,
                        'latitude' => $details['latitude'],
                        'longitude' => $details['longitude'],
                        'location_name' => $details['location_name'],
                        'location_address' => $details['location_address'],
                        'location_url' => $details['location_url'],
                        'sender' => $sender,
                    ];

                    $url = config('unofficial-whatsapp.api.endpoint') . '/sendLocation';
                    break;

                case self::MESSAGE_TYPE_BUTTONS:
                    $details = $this->details;
                    $requestData = [
                        'recipient' => $this->recipient,
                        'buttons' => json_encode($details['buttons']),
                        'button_title' => $details['button_title'],
                        'button_body' => $details['button_body'],
                        'button_footer' => $details['button_footer'],
                        'sender' => $sender,
                    ];

                    $url = config('unofficial-whatsapp.api.endpoint') . '/sendButtons';
                    break;

                case self::MESSAGE_TYPE_POLL:
                    $details = $this->details;
                    $requestData = [
                        'recipient' => $this->recipient,
                        'question' => $details['question'],
                        'answers' => json_encode($details['answers']),
                        'allow_multiple_answers' => $details['allow_multiple_answers'],
                        'sender' => $sender,
                    ];

                    $url = config('unofficial-whatsapp.api.endpoint') . '/sendPoll';
                    break;

                case self::MESSAGE_TYPE_VCARD:
                    $details = $this->details;
                    $requestData = [
                        'recipient' => $this->recipient,
                        'contact_name' => $details['contact_name'],
                        'contact_number' => $details['contact_number'],
                        'sender' => $sender,
                    ];

                    $url = config('unofficial-whatsapp.api.endpoint') . '/sendContactCard';
                    break;
            }

            $session = curl_init($url);

            curl_setopt($session, CURLOPT_POST, true);
            curl_setopt($session, CURLOPT_POSTFIELDS, $requestData);

            curl_setopt($session, CURLOPT_HTTPHEADER, [
                'x-api-key: ' . config('unofficial-whatsapp.api.key'),
                'x-api-secret: ' . config('unofficial-whatsapp.api.secret')
            ]);

            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($session, CURLOPT_FOLLOWLOCATION, true);
            $response = curl_exec($session);

            curl_close($session);

            $response = json_decode($response, true);

            if (!empty($response)) {
                if (isset($response['id'])) {
                    $this->queued = true;
                    $this->gateway_id = $response['id'];
                    $this->queued_at = now();
                    $this->save();
                }

                return true;
            }

            return false;
        }
    }

    public static function checkWebhookSignature()
    {
        $payload = request()->get('payload');
        $signature = request()->get('signature');
        $string = config('unofficial-whatsapp.api.key') . '---' . trim(json_encode($payload['hash']), '{}') . '---' . config('unofficial-whatsapp.api.secret');
        $calculatedSignature = md5($string);

        if ($signature != $calculatedSignature) {
            throw new \Exception('Wrong signature');
        }
    }

    public static function updateFromPayload($payload)
    {
        $message = static::query()
            ->where('gateway_id', $payload['message']['id'])
            ->first();

        if (is_null($message)) {
            return false;
        }

        $message->sender = $payload['message']['sender'];
        $message->sent = $payload['message']['sent'];
        $message->failed = $payload['message']['failed'];
        $message->delivered = $payload['message']['delivered'];
        $message->read = $payload['message']['read'];

        if (isset($payload['message']['replay_message_id'])) {
            $message->reply_message_id = $payload['message']['replay_message_id'];
        }

        if (isset($payload['message']['reaction'])) {
            $message->reaction = $payload['message']['reaction'];
        }

        if (isset($payload['message']['has_media'])) {
            $message->has_media = $payload['message']['has_media'];
        }

        if (isset($payload['message']['media_url'])) {
            $message->media_url = $payload['message']['media_url'];
        }

        if (isset($payload['message']['queued_at'])) {
            $message->queued_at = $payload['message']['queued_at'];
        }

        if (isset($payload['message']['sent_at'])) {
            $message->sent_at = $payload['message']['sent_at'];
        }

        if (isset($payload['message']['delivered_at'])) {
            $message->delivered_at = $payload['message']['delivered_at'];
        }

        if (isset($payload['message']['read_at'])) {
            $message->read_at = $payload['message']['read_at'];
        }

        $message->save();

        return $message;
    }

    public static function createFromPayload($payload)
    {
        $message = new static();
        $message->gateway_id = $payload['message']['id'];
        $message->type = $payload['message']['type'];
        $message->recipient = $payload['message']['recipient'];
        $message->sender = $payload['message']['sender'];
        $message->content = $payload['message']['content'];
        $message->queued = $payload['message']['queued'];
        $message->sent = $payload['message']['sent'];
        $message->failed = $payload['message']['failed'];
        $message->delivered = $payload['message']['delivered'];
        $message->read = $payload['message']['read'];
        $message->has_media = $payload['message']['has_media'];
        if (isset($payload['message']['media_url'])) {
            $message->media_url = $payload['message']['media_url'];
        }
        if (isset($payload['message']['reaction'])) {
            $message->reaction = $payload['message']['reaction'];
        }
        $message->details = [];
        $message->created_at = now();
        $message->sent_at = $payload['message']['sent_at'];
        $message->delivered_at = $payload['message']['delivered_at'];
        $message->read_at = $payload['message']['read_at'];
        $message->save();

        return $message;
    }

    public static function processRequest()
    {
        try {
            static::checkWebhookSignature();
        } catch (\Exception $e) {
            return response()->json([
                'error' => [
                    'message' => 'Invalid signature - unauthorized',
                    'status_code' => 403
                ]
            ], 403);
        }

        $type = request()->get('type');
        $payload = request()->get('payload');

        switch ($type) {
            case self::WEBHOOK_TYPE_WEBHOOK_TEST:
                return response()->json(['tested' => true]);
                break;

            case self::WEBHOOK_TYPE_MESSAGE_QUEUED:
                return response()->json(['success' => static::updateFromPayload($payload)]);
                break;

            case self::WEBHOOK_TYPE_DELIVERY_REPORT:
                $payload = static::updateFromPayload($payload);

                if (method_exists(static::class, 'messageReport')) {
                    static::messageReport($payload);
                }

                return response()->json(['success' => (bool)$payload]);
                break;

            case self::WEBHOOK_TYPE_MESSAGE_RECEIVED:
                $payload = static::createFromPayload($payload);

                if (method_exists(static::class, 'messageReceived')) {
                    static::messageReceived($payload);
                }

                return response()->json(['success' => (bool)$payload]);
                break;

            default:
                return response()->json([
                    'error' => [
                        'message' => 'Invalid type',
                        'status_code' => 422
                    ]
                ], 422);
                break;
        }
    }
}
