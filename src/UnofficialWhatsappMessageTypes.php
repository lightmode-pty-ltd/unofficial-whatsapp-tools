<?php

namespace UnofficialWhatsappTools;

use Illuminate\Database\Eloquent\Model;

class UnofficialWhatsappMessageTypes extends Model
{
    const MESSAGE_TYPE_TEXT = 1;
    const MESSAGE_TYPE_MEDIA = 2;
    const MESSAGE_TYPE_LOCATION = 3;
    const MESSAGE_TYPE_BUTTONS = 4;
    const MESSAGE_TYPE_POLL = 5;
    const MESSAGE_TYPE_VCARD = 6;


    const MESSAGE_TYPE_IMAGE = 20;
    const MESSAGE_TYPE_VIDEO = 21;
    const MESSAGE_TYPE_PTT = 22;
    const MESSAGE_TYPE_DOCUMENT = 23;
    const MESSAGE_TYPE_AUDIO = 24;
    const MESSAGE_TYPE_STICKER = 25;

    const MESSAGE_TYPE_SYSTEM = 90;
    const MESSAGE_TYPE_AUTOMATED = 91;

    const MESSAGE_TYPE_UNKNOWN = 99;

}
