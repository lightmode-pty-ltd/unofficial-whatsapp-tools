<?php

namespace UnofficialWhatsappTools;

final class UnofficialWhatsappComponent
{
    /**
     * Parameters of a header template.
     */
    private $header;

    /**
     * Parameters of a body template.
     */
    private $body;

    /**
     * Buttons to attach to a template.
     */
    private $buttons;

    public function __construct(array $header = [], array $body = [], array $buttons = [])
    {
        $this->header = $header;
        $this->body = $body;
        $this->buttons = $buttons;
    }

    public function header()
    {
        return $this->header;
    }

    public function body()
    {
        return $this->body;
    }

    public function buttons()
    {
        return $this->buttons;
    }

    public function toArray()
    {
        return [
            'header' => $this->header,
            'body' => $this->body,
            'buttons' => $this->buttons
        ];
    }
}

