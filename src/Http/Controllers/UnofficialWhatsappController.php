<?php

namespace UnofficialWhatsappTools\Http\Controllers;

class UnofficialWhatsappController extends \App\Http\Controllers\Controller
{
    public function webhook()
    {
        $messageClass = config('unofficial-whatsapp.message_model');
        $messageInstance = new $messageClass();

        return $messageInstance::processRequest();
    }
}
