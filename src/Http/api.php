<?php

$router->post(config('unofficial-whatsapp.router.webhookEndpoint'), ['uses' => 'UnofficialWhatsappController@webhook', 'as' => config('unofficial-whatsapp.router.namedPrefix') . '.webhook']);

