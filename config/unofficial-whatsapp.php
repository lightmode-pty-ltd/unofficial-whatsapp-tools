<?php

return [
    'router' => [
        'includeRoutes' => true,
        'prefix' => 'unofficialwhatsapptools',
        'namedPrefix' => 'unofficial-whatsapp-tools',
        'webhookEndpoint' => 'webhook',
        'middleware' => [],
    ],

    'api' => [
        'endpoint' => env('UNOFFICIAL_WHATSAPP_API_ENDPOINT', 'https://uwa.lightmode.co.za/api/v1/whatsapp'),
        'key' => env('UNOFFICIAL_WHATSAPP_API_KEY'),
        'secret' => env('UNOFFICIAL_WHATSAPP_API_SECRET'),
        'webhook' => env('UNOFFICIAL_WHATSAPP_API_WEBHOOK'),

        'number' => env('UNOFFICIAL_WHATSAPP_API_DEFAULT_NUMBER', null)
    ],

    'message_model' => \App\UnofficialWhatsappMessage::class,
];

