# whatsappTools
Unofficial Whatsapp Tools

## Contents
1. Intro
2. Examples

# 1. Intro

## How to install?

- composer require lightmode/unofficial-whatsapp-tools
- php artisan vendor:publish
- php artisan migrate
- check config/whatsapp.php (just in case)
- add your API details to .env
- php artisan unofficial-whatsapptools:setup
- check the examples below
- enjoy! 

## Samples

### .env sample config

UNOFFICIAL_WHATSAPP_API_ENDPOINT="https://uwa.lightmode.co.za/api/v1/whatsapp"
UNOFFICIAL_WHATSAPP_API_KEY="AAAAAAAAA"
UNOFFICIAL_WHATSAPP_API_SECRET="ZZZZZZZZZZZZZZZZZZZ"


## Examples

### Sending a text message
\App\UnofficialWhatsappMessage::queueTextMessage('2779xxxxxxx89', 'Hello text world!');
